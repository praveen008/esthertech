from django.shortcuts import render, HttpResponse

from . models import Post


def bloghome(request):
    posts = Post.objects.all()
    context = {'posts': posts}
    return render(request, 'blog/blog_layout=list.html', context)

def blogpost(request, slug):
    return HttpResponse(f'html my blog Post page:{slug}')